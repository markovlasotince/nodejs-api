******
******
******
* Open project

* In terminal type : 
	npm install --save express jsonwebtoken mongoose body-parser
	npm install -g nodemon

Run: 
	nodemon server.js

Make sure your MongoDB is running on localhost.
Default PORT 27017

******

POSTMAN

***ADMIN***

	*Add admin: 

Method -> POST -> http://localhost:3000/admins/register
Data -> {
	"email" : "marko@aadmin.com",
	"password" : "Qwerty123456"
}

	*Check if there is admin (auth) and if there is return 200 Status and set JWT in Authorization Header:

Method -> POST -> http://localhost:3000/admins/auth
Data-> {
	"email" : "marko@aadmin.com",
	"password" : "Qwerty123456"
}

***POSITION***
Note: Put auth token in headers(Working with positions requires authentification).

	*Add position :
Method -> POST -> http://localhost:3000/positions
Data -> {
	"name":"Junior Develope",
"description":"Lorem ipsum olor sit amet",
"startDate":"2019-05-06T22:00:00.000+00:00",
"endDate":"2019-07-08T22:00:00.000+00:00"
} 

	*Get all positions:
Method -> GET -> http://localhost:3000/positions

	*Get one position:
Method -> GET -> http://localhost:3000/positions/${id}

	*Edit position:
Method -> PUT -> http://localhost:3000/positions/${id}
Data -> {
	"name":"Senior Develope",
"description":"Lorem ipt"
}
	
	*Delete position:
Method -> DELETE -> http://localhost:3000/positions/${id}

***APPLICANTS***

	*Add applicant:
Method -> POST -> http://localhost:3000/applicants
Data -> {
	"name":"Marko Popovic",
	"email":"marko@gmail.cccom",
	"phone":"1244543",
	"education": "other",
	"positionId": "5ced96d6ce22ff1a9cc59ac7"
}

	*Get all applicants
Method -> GET -> http://localhost:3000/applicants

	*Get one applicant
Method -> GET -> http://localhost:3000/applicants/${}

	*Edit applicant
Method -> PUT -> http://localhost:3000/applicants/${}
Data -> {
	"name":"Petar Petrovic",
	"email":"marko@gmail.cccom",
	"phone":"1244543",
	"education": "bachelor"
}
	
	*Delete applicant
Method -> DELETE -> http://localhost:3000/applicants/${}

