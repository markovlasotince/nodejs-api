const express = require("express");
const router = express.Router();
const applicantsController = require("../controllers/applicants");

const multer = require("multer");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    console.log(req.headers);
    cb(null, "uploads");
  },
  filename: function(req, file, cb) {
    var date = new Date();
    console.log(file);
    var x = file.originalname;
    cb(null, x);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === "application/pdf" || file.mimetype === "pdf") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// const multer = require('multer')
// const upload = multer({dest: 'uploads/'});

// router.post("/", upload.single('files') ,applicantsController.createApplicant);
router.post("/", applicantsController.createApplicant);
router.get("/", applicantsController.getAllApplicants);
router.get("/:applicantId", applicantsController.getAllApplicantById);
router.patch("/:applicantId", applicantsController.editApplicantById);
router.delete("/:applicantId", applicantsController.deleteApplicantById);
router.delete("/", applicantsController.deleteAllApplicants);

module.exports = router;
