const express = require("express");
const router = express.Router();
const positionController = require("../controllers/positions");

router.post("/", positionController.createPosition);
router.get("/", positionController.getAllPositions);
router.get("/valid", positionController.getValidPostions);
router.get("/:positionId", positionController.getPositionById);
router.patch("/:positionId", positionController.editPositionById);
router.delete("/:positionId", positionController.deletePositionById);

module.exports = router;
