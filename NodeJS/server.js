const app = require("./app");
const port = require("./config/port");

// app.get("/proba", function(req, res) {
//   res.send({ tutorial: "Build REST API with node.js" });
// });

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

// const express = require("express");
// const cors = require("cors");
// const logger = require("morgan");
// const admins = require("./routes/admins");
// const position = require("./routes/positions");
// const applicants = require("./routes/applicants");
// const bodyParser = require("body-parser");
// const mongoose = require("./config/database"); //database configuration
// const PORT = require("./config/port");
// const httpStatus = require("./config/httpHandle");

// var jwt = require("jsonwebtoken");
// const app = express();

// app.use(
//   cors({
//     exposedHeaders: ["Content-Type", "Authorization"]
//   })
// );

// connection to mongodb
// mongoose.connection.on(
//   "error",
//   console.error.bind(console, "MongoDB connection error:")
// );
// mongoose.set("useFindAndModify", false);

// app.use("/uploads", express.static("uploads"));

// app.set("secretKey", "nodeRestApi"); // jwt secret token
// app.use(logger("dev"));

// bodyParser
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
// app.use(function(req, res, next) {
//   res.setHeader("Content-Type", "application/json");
//   next();
// });

// Cors
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept, Z-Key"
//   );
//   res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//   next();
//

// ***************

// var multer = require("multer");
// var storage = multer.diskStorage({
//   destination: function(req, file, callback) {
//     callback(null, "./uploads");
//   },
//   filename: function(req, file, callback) {
//     callback(null, file.fieldname + "-" + Date.now());
//   }
// });
// var upload = multer({ storage: storage }).single("file");

// app.get("/", function(req, res) {
//   //res.sendFile(__dirname + "/index.html");
// });

// app.post("/api/photo", function(req, res) {
//   upload(req, res, function(err) {
//     if (err) {
//       return res.end("Error uploading file.");
//     }
//     res.end("File is uploaded");
//   });
// });

// ***************

// public routes
// app.use("/admins", admins);
// app.use("/applicants", validateAdmin, applicants);
// app.use("/applicants", applicants);

// unprotected route
// app.use("/positions", position);
// protected route
// app.use("/positions", validateAdmin, position);

// app.get("/favicon.ico", function(req, res) {
//   res.sendStatus(204);
// });

// function validateAdmin(req, res, next) {
//   if (req.headers.authorization) {
//     const token = req.headers.authorization.split(" ");
//     jwt.verify(token[1], req.app.get("secretKey"), function(err, decoded) {
//       if (err) {
//         res.status(403).send(httpStatus.forbidden);
//       } else {
//         // add user id to request
//         req.body.userId = decoded.id;
//         next();
//       }
//     });
//   } else {
//     res.status(403).send(httpStatus.forbidden);
//   }
// }

// express doesn't consider not found 404 as an error so we need to handle 404 it explicitly
// handle 404 error
// app.use(function(req, res, next) {
//   let err = new Error("Not Found");
//   err.status = 404;
//   next(err);
// });

// handle errors
// app.use(function(err, req, res, next) {
//   if (err.status === 404) res.status(404).json({ message: "Not found" });
//   else res.status(500).json({ message: "Something looks wrong :( !!!" });
// });

// app.listen(PORT, function() {
//   console.log("Node server listening on port 3000");
// });
