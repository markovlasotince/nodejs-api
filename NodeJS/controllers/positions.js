const positionModel = require("../models/positions");

module.exports = {
  createPosition: async function(req, res) {
    const position = new positionModel(req.body);
    try {
      await position.save();
      res.status(201).send(position);
    } catch (error) {
      res.status(400).send(error);
    }
  },
  getAllPositions: async function(req, res, next) {
    try {
      const positions = await positionModel.find({});
      res.status(200).send(positions);
    } catch (error) {
      res.status(500).send(error);
    }
    // let mySort = {};
    // if (
    //   Object.entries(req.query).length === 0 &&
    //   req.query.constructor === Object
    // ) {
    //   mySort.name = 1;
    // } else {
    //   console.log(req.query.direction);
    //   //var tpm = req.query.search.split(">");
    //  // console.log(tpm);
    //   req.query.direction === "asc"
    //     ? (mySort[req.query.sort] = 1)
    //     : (mySort[req.query.sort] = -1);
    //   console.log(mySort);
    // }
    // await postionModel
    //   .find({}, function(err, positions) {
    //     if (err) {
    //       next(err);
    //     }
    //           //var tpm = req.query.search.split(">");
    //  console.log(req.query);
    //     if (positions && positions.length > 0) {
    //       console.log(positions);
    //       var date = new Date();
    //       var x = date.getTime();
    //       console.log(x)
    //       let allApplicants = httpStatus.success;
    //       if(req.query.search){
    //         allApplicants.positions = positions.filter(position => {
    //           let positionDate = new Date(position.startDate.toString());
    //           return positionDate.getTime() > x;
    //         });
    //       } else {
    //         allApplicants.positions = positions;
    //       }
    //       // allApplicants.data = {tmp};
    //       allApplicants.path = `/positions`;
    //       res.status(200).send(allApplicants);
    //     } else {
    //       res.status(404).send(httpStatus.notFound);
    //     }
    //   })
    //   .sort(mySort);
  },
  getValidPostions: async function(req, res) {
    const date = new Date();
    const positions = await positionModel.find({
      endDate: { $gte: date }
    });
    if (!positions) return res.status(404).send();
    res.status(200).send(positions);
  },
  getPositionById: async function(req, res) {
    try {
      const position = await positionModel.findById(req.params.positionId);
      if (!position) return res.status(404).send();
      res.status(200).send(position);
    } catch (error) {
      res.status(404).send(error);
    }
  },
  editPositionById: async function(req, res) {
    const _id = req.params.positionId;
    const updates = Object.keys(req.body);
    const allowedUpdates = ["name", "description"];

    const isValidOperation = updates.every(update =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation)
      return res.status(400).send({ error: "Invalid property for updating" });

    try {
      const position = await positionModel.findById(_id);
      if (!position) return res.status(404);

      updates.forEach(update => {
        position[update] = req.body[update];
      });

      await position.save();
      res.status(200).send(position);
    } catch (e) {
      res.status(500).send(e);
    }
  },
  deletePositionById: async function(req, res) {
    try {
      await positionModel.findByIdAndRemove(
        req.params.positionId,
        (err, positionInfo) => {
          if (err) {
            res.status(404).send(err);
          } else {
            if (positionInfo) {
              res.status(200).send(positionInfo);
            } else {
              res.status(404).send();
            }
          }
        }
      );
    } catch (error) {
      res.status(500).send(error);
    }
  }
};
