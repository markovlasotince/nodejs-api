const adminModel = require("../models/admins");

module.exports = {
  create: async function(req, res, next) {
    const admin = new adminModel(req.body);
    try {
      const token = await admin.generateJWT();
      await admin.save();
      res.status(201).send({ admin, token });
    } catch (e) {
      res.status(400).send(e);
    }
  },
  authenticate: async function(req, res, next) {
    try {
      const admin = await adminModel.findByCredentials(
        req.body.email,
        req.body.password
      );
      const token = await admin.generateJWT();
      admin.save();
      res.status(200).send({ admin, token });
    } catch (error) {
      res.status(400).send(error);
    }
  }
};
