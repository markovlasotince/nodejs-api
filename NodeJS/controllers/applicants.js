const applicanModel = require("../models/applicants");

module.exports = {
  createApplicant: async function(req, res, next) {
    const applicant = new applicanModel(req.body);
    await applicant.save();
    res.status(201).send(applicant);
    try {
    } catch (error) {
      res.status(500).send(error);
    }
  },
  getAllApplicants: async function(req, res) {
    let mySort = {};
    if (
      Object.entries(req.query).length === 0 &&
      req.query.constructor === Object
    ) {
      mySort.name = 1;
    } else {
      req.query.direction === "asc"
        ? (mySort[req.query.sort] = 1)
        : (mySort[req.query.sort] = -1);
    }
    await applicanModel
      .find({}, function(err, result) {
        if (err) {
          return res.status(500).send(err);
        }
        if (result && result.length > 0) {
          res.status(200).send(result);
        } else {
          res.status(404).send();
        }
      })
      .populate("positionId")
      .sort(mySort);
  },
  getAllApplicantById: function(req, res) {
    applicanModel.findById(req.params.applicantId, function(err, applicant) {
      if (err) {
        return res.status(500).send(err);
      } else {
        if (applicant) {
          res.status(200).send(applicant);
        } else {
          res.status(404).send();
        }
      }
    });
  },
  editApplicantById: function(req, res, next) {
    applicanModel.findOneAndUpdate(
      { _id: req.params.applicantId },
      { $set: req.body },
      { new: true },
      function(err, applicant) {
        if (err) return res.status(500).send(err);
        if (applicant) {
          res.status(200).send(applicant);
        } else {
          res.status(404).send();
        }
      }
    );
  },
  deleteApplicantById: function(req, res) {
    applicanModel.findByIdAndRemove(
      req.params.applicantId,
      (err, applicantInfo) => {
        if (err) return res.status(500).send();

        if (!applicantInfo) return res.status(404).send();

        res.status(200).send(applicantInfo);
      }
    );
  },
  deleteAllApplicants: function(req, res) {
    applicanModel.deleteMany({}, (err, result) => {
      if (err) return res.status(500).send(err);
      res.status(200).send(result);
    });
  }
};
