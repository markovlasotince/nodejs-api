// const forbidden = {
//   timestamp: new Date(),
//   status: 403,
//   error: "Forbidden",
//   message: "Access Denied",
//   path: "/api/"
// };

module.exports = {
  forbidden: {
    timestamp: new Date(),
    status: 403,
    error: "Forbidden",
    message: "Access Denied",
    path: "/api/"
  },
  created: {
    timestamp: new Date(),
    status: 201,
    message: "Created",
    path: "/api/"
  },
  success: {
    timestamp: new Date(),
    status: 200,
    message: "Success",
    path: "/api/"
  },
  notFound: {
    timestamp: new Date(),
    status: 404,
    message: "Not Found",
    path: "/api/"
  },
  badRequest: {
    timestamp: new Date(),
    status: 400,
    message: "Bad request",
    path: "/api/"
  }
};
