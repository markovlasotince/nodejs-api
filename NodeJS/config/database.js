//Set up mongoose connection
console.log("in db config");
const mongoose = require("mongoose");
const mongoDB = "mongodb://localhost:27017/myadmins";

mongoose.set("useFindAndModify", false);
mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useCreateIndex: true
});

// mongoose.connect(mongoDB,{ useNewUrlParser: true });
// mongoose.Promise = global.Promise;

// module.exports = mongoose;
