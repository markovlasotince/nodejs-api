const express = require("express");
require("./config/database");
const adminRouter = require("./routes/admins");
const positionsRouter = require("./routes/positions");
const applicantRouter = require("./routes/applicants");

const app = express();

app.use(express.json());
app.use("/admins", adminRouter);
app.use("/positions", positionsRouter);
app.use("/applicants", applicantRouter);

module.exports = app;
