const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ApplicantsSchema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true
    },
    email: {
      type: String,
      trim: true,
      unique: true,
      required: true,
      lowercase: true
    },
    phone: {
      type: String,
      trim: true
    },
    education: {
      type: String,
      trim: true
      // required: true,
    },
    positionId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Position"
    },
    applicantsStatus: {
      type: String,
      default: "REJECTED",
      trim: true
    },
    productImage: {
      type: String,
      trim: true
    },
    fileName: {
      type: String,
      trim: true
    },
    fileSize: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

const Applicants = mongoose.model("Applicants", ApplicantsSchema);

module.exports = Applicants;
