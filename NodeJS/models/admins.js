const mongoose = require("mongoose");
const validator = require("validator");
const jwt = require("jsonwebtoken");

const Schema = mongoose.Schema;
const AdminSchema = new Schema(
  {
    email: {
      type: String,
      trim: true,
      required: true,
      lowercase: true,
      unique: true,
      dropDups: true,
      validate(value) {
        if (!validator.isEmail(value)) throw new Error("Email is not valid");
      }
    },
    password: {
      type: String,
      trim: true,
      required: true,
      minlength: 7,
      validate(value) {
        if (value.toLowerCase().includes("password"))
          throw new Error("Password contains word 'password'.");
      }
    },
    username: {
      type: String,
      trim: true
    },
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  {
    timestamps: true
  }
);

AdminSchema.methods.generateJWT = async function() {
  const user = this;
  const jtoken = jwt.sign({ _id: user._id.toString() }, "mysecretJWTkey", {
    expiresIn: "15 minutes"
  });
  user.tokens = user.tokens.concat({ token: jtoken });
  await user.save();
  return jtoken;
};

AdminSchema.methods.toJSON = function() {
  const admin = this;
  const adminObject = admin.toObject();

  delete adminObject.password;
  delete adminObject.tokens;

  return adminObject;
};

AdminSchema.statics.findByCredentials = async (email, password) => {
  const admin = await Admin.findOne({ email });
  if (!admin) {
    throw new Error("Unable to login");
  }

  if (admin.password !== password) {
    throw new Error("Unable to login");
  }

  return admin;
};

const Admin = mongoose.model("Admin", AdminSchema);
module.exports = Admin;
