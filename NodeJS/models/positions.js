const mongoose = require("mongoose");
const Applicants = require("./applicants");

const Schema = mongoose.Schema;

const PositionSchema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true
    },
    description: {
      type: String,
      trim: true,
      required: true
    },
    startDate: {
      type: Date,
      trim: true,
      required: true
    },
    endDate: {
      type: Date,
      trim: true,
      required: true
    }
  },
  {
    timestamps: true
  }
);

// Virtual atributes
PositionSchema.virtual("Applicants", {
  ref: "Applicants",
  localField: "_id",
  foreignField: "positionId"
});

// delete all applicants when position is deleted
PositionSchema.pre("remove", async function(next) {
  const position = this;
  console.log(position._id)
  await Applicants.deleteMany({ positionId: position._id });
  next();
});

const Position = mongoose.model("Position", PositionSchema);

module.exports = Position;
